import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.java.archives.internal.DefaultManifest
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.islandoftex.albatross.build.CTANBuilderTask
import org.islandoftex.albatross.build.TDSZipBuilderTask

plugins {
    kotlin("jvm") version "1.7.21"
    kotlin("plugin.serialization") version "1.7.21"
    application
    id("com.github.ben-manes.versions") version "0.43.0"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("com.diffplug.spotless") version "6.11.0"
    id("com.diffplug.spotless-changelog") version "2.4.0"
    id("io.gitlab.arturbosch.detekt") version "1.21.0"
}

group = "org.islandoftex"
val moduleName = "$group.${project.name}"

repositories {
    mavenCentral()
}

dependencies {
    implementation("net.harawata:appdirs:1.2.1")
    implementation("com.github.ajalt.mordant:mordant:2.0.0-beta8")
    implementation("com.github.ajalt.clikt:clikt:3.5.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-protobuf:1.4.1")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_9
    targetCompatibility = JavaVersion.VERSION_1_9
}

sourceSets {
    main {
        java { setSrcDirs(listOf("src/main/kotlin")) }
        resources { setSrcDirs(listOf("src/main/resources")) }
    }
    test {
        java { setSrcDirs(listOf("src/test/kotlin")) }
        resources { setSrcDirs(listOf("src/test/resources")) }
    }
}

application {
    applicationName = rootProject.name
    mainClass.set("$moduleName.ApplicationKt")
}

spotlessChangelog {
    changelogFile("CHANGELOG.md")
    setAppendDashSnapshotUnless_dashPrelease(true)
    ifFoundBumpBreaking("breaking change")
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

version = spotlessChangelog.versionNext
allprojects {
    repositories {
        mavenCentral()
    }

    group = "org.islandoftex.albatross"
    version = rootProject.version
}


spotless {
    kotlinGradle {
        target("build.gradle.kts",
               "buildSrc/build.gradle.kts")
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    kotlin {
        target("buildSrc/src/**/*.kt",
               "src/**/*.kt")
        ktlint()
        licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
}

val appManifest: Manifest by extra(DefaultManifest(
    (project as ProjectInternal)
        .fileResolver
).apply {
    attributes["Implementation-Title"] = project.name
    attributes["Implementation-Version"] = project.version
    attributes["Main-Class"] = "$moduleName.ApplicationKt"
    if (java.sourceCompatibility < JavaVersion.VERSION_1_9) {
        attributes["Automatic-Module-Name"] = moduleName
    }
})

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "9"
    }
    named<Jar>("jar") {
        manifest.attributes.putAll(appManifest.attributes)
        archiveAppendix.set("jdk" + java.targetCompatibility.majorVersion)
    }
    named<ShadowJar>("shadowJar") {
        manifest.attributes.putAll(appManifest.attributes)
        archiveAppendix.set("jdk" + java.targetCompatibility.majorVersion + "-with-deps")
        archiveClassifier.set("")
        minimize()

        // exclude darwin-specific JNA dependency; JNA is only required by appdirs
        // but not used on darwin which would require notarization of the compiled
        // code that it does not have
        exclude("com/sun/jna/darwin/*")
    }
    named<DependencyUpdatesTask>("dependencyUpdates") {
        resolutionStrategy {
            componentSelection {
                all {
                    fun isNonStable(version: String) = listOf("alpha", "beta", "rc", "cr", "m", "preview", "b", "ea")
                        .any { qualifier ->
                            version.matches(Regex("(?i).*[.-]$qualifier[.\\d-+]*"))
                        }
                    if (isNonStable(candidate.version) && !isNonStable(currentVersion)) {
                        reject("Release candidate")
                    }
                }
            }
        }
        checkForGradleUpdate = false
    }
    named<Task>("assembleDist").configure { dependsOn("shadowJar") }

    register<TDSZipBuilderTask>("assembleTDSZip")
    register<CTANBuilderTask>("assembleCTAN") {
        dependsOn(":assembleTDSZip")
    }
}

detekt {
    ignoreFailures = false
    buildUponDefaultConfig = true
    config = files("detekt-config.yml")
}
