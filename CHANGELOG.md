# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.1] - 2024-02-13

### Changed

* This release does not contain the JNA code for Apple's darwin platform
  anymore due to code signing issues. While we could not test the effectiveness
  and usability of the result, the MacTeX maintainer already uses this strategy
  to distribute albatross. If there are any issues, please report and we will
  revert until we receive contributions with a proper fix.
  (see #13)

## [0.5.0] - 2023-01-27

### Added

* Experimental font look up in the current TeX tree has been added.
  (see #2)
* The new border style (none, `-b0`) hides the border of the tables entirely.
  (see #9)
* Support for graphemes has been added. Now, `albatross y̆` will look for fonts
  that contain both `U+79` and `U+306` (conjunctive behavior). Note that, when
  searching for multiple glyphs using the `--or` modifier (disjunctive behavior),
  grapheme elements always will rely on conjunctive behavior. 
  (see #10)

### Changed

* Table for font names (default view) has no internal borders now.
  (see #9)
* Bump required Java version to 9. This drops support for Java 8.
  (breaking change)

### Fixed

* The conjunctive behavior introduce in 0.3.0 has not been working as intended.
  Now it works as documented.
  (see #4)

## [0.4.0] - 2021-11-22

### Added

* Included support for the Unicode code point using the `U+` multiset union
  notation, which behaves exactly the same as the `0x` counterpart. So `ß`,
  `0xDF` and `U+DF` denote the same Unicode entity.
  (see #5)

### Fixed

* The help page did not mention the tool's purpose.
  (see #6) 

## [0.3.0] - 2021-01-13

### Changed

* Conjunctive behavior is now default. Previously, `albatross a b` would have
  looked for fonts for `a` and separately for fonts for `b`. As we see more
  use cases for looking for fonts that contain `a` as well as `b`, we changed
  the default and left the previous behavior as `albatross --or a b`.
  (breaking change, see #4)

## [0.2.0] - 2020-12-09

### Added

* Inclusion of a man page.
  (see #1)

### Fixed

* Windows paths were incorrectly parsed, causing font names and styles to be
  displayed incorrectly.
  (see #3)

## [0.1.0] - 2020-12-07

* Initial release

[Unreleased]: https://gitlab.com/islandoftex/albatross/compare/v0.5.1...master
[0.5.1]: https://gitlab.com/islandoftex/albatross/-/tags/v0.5.1
[0.5.0]: https://gitlab.com/islandoftex/albatross/-/tags/v0.5.0
[0.4.0]: https://gitlab.com/islandoftex/albatross/-/tags/v0.4.0
[0.3.0]: https://gitlab.com/islandoftex/albatross/-/tags/v0.3.0
[0.2.0]: https://gitlab.com/islandoftex/albatross/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/islandoftex/albatross/-/tags/v0.1.0
