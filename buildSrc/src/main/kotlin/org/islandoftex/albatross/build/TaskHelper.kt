// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross.build

import org.zeroturnaround.exec.ProcessExecutor
import java.io.File
import java.io.IOException
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object TaskHelper {
    /**
     * Checks whether the commands are available in the system path.
     * @param commands Array of commands to be checked.
     * @throws IOException The command does not exist.
     */
    @Throws(IOException::class)
    fun assertAvailability(vararg commands: String) {
        try {
            @Suppress("SpreadOperator")
            execute(File("."), *commands)
        } catch (_: IOException) {
            throw IOException(
                "The command has returned an invalid " +
                    "exit value. Chances are the command is not " +
                    "available in the system path. Make sure the " +
                    "command exists and try again. The application " +
                    "will halt now."
            )
        }
    }

    /**
     * Executes the command and arguments in the provided directory.
     * @param directory The working directory.
     * @param call The proper call with the command and arguments.
     * @throws IOException An error has occurred during the execution.
     */
    @Throws(IOException::class)
    fun execute(directory: File, vararg call: String) {
        try {
            ProcessExecutor().command(call.toList()).directory(directory)
                .exitValueNormal().execute()
        } catch (_: Exception) {
            throw IOException(
                "The command call has returned an " +
                    "invalid exit value. Chances are the arguments are " +
                    "incorrect. Make sure the call contains valid arguments " +
                    "and try again."
            )
        }
    }

    /**
     * Creates a shell script file for albatross.
     * @param file The file reference.
     * @throws IOException The file could not be written.
     */
    @Throws(IOException::class)
    fun createScript(file: File) {
        try {
            file.writeText(
                """
                #!/bin/sh
                # Public domain. Originally written by Norbert Preining and Karl Berry, 2018.
                # Note from Paulo: this script provides better Cygwin support than our original
                # approach, so the team decided to use it as a proper wrapper for albatross as well.

                scriptname=`basename "$0"`
                jar="${'$'}scriptname.jar"
                jarpath=`kpsewhich --progname="${'$'}scriptname" --format=texmfscripts "${'$'}jar"`

                kernel=`uname -s 2>/dev/null`
                if echo "${'$'}kernel" | grep CYGWIN >/dev/null; then
                  CYGWIN_ROOT=`cygpath -w /`
                  export CYGWIN_ROOT
                  jarpath=`cygpath -w "${'$'}jarpath"`
                fi

                exec java -jar "${'$'}jarpath" "${'$'}@"
                """.trimIndent() + "\n"
            )
        } catch (_: IOException) {
            throw IOException(
                "I could not create the shell script for " +
                    "albatross due to an IO error. Please make sure the " +
                    "current directory has the correct permissions " +
                    "and try again. The application will halt now."
            )
        }
    }

    /**
     * Creates a man page file for albatross.
     * @param file The file reference.
     * @param version The tool version.
     * @throws IOException The file could not be written.
     */
    @Throws(IOException::class)
    fun createManPage(file: File, version: String) {
        val today = LocalDate.now().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))
        try {
            file.writeText(
                """
                .TH ALBATROSS 1 "$today" "v$version"
                .
                .SH NAME
                albatross \- a tool for finding fonts that contain a given (Unicode) glyph.
                .
                .SH SYNOPSIS
                .B albatross
                .RI [ options ]
                .IR glyphs ...
                .
                .SH DESCRIPTION
                \fBalbatross\fP is a command line tool for finding fonts that contain a given
                (Unicode) glyph.
                It relies on Fontconfig, a library for configuring and customizing font
                access.
                .PP
                Albatross takes a list of glyphs as input.
                Three formats are supported:
                .IP #1
                The glyph itself, e.g., \fBß\fP (Eszett).
                Internally, the tool converts it to the corresponding Unicode code point.
                .IP #2
                The glyph as a Unicode code point in the hexadecimal notation,
                e.g., \fB0xDF\fP.
                The \fB0x\fP prefix is mandatory.
                Note that the tool takes the value as case insensitive, e.g.,
                \fB0xDF\fP is equal to \fB0xdf\fP (or any case combination thereof).
                .IP #3
                The glyph as a Unicode code point using the multiset union notation,
                e.g, \fBU+DF\fP.
                The \fBU+\fP prefix is mandatory.
                Be mindful that this notation expects an uppercase \fBU\fP.
                .
                .SH OPTIONS
                .IP \fB--show-styles\fP
                Show available font styles
                .IP \fB--detailed\fP
                Show a detailed font list
                .IP "\fB--ansi-level\fP [n|a16|a256|tc]"
                Set the default ANSI level
                .IP "\fB--border-style\fP [0|1|2|3|4|5|6]"
                Set the border style
                .IP \fB--or\fP
                Look for each glyph separately
                .IP \fB--version\fP
                Show the version and exit
                .IP \fB--help\fP
                Show a help message and exit
                .
                .SH BUGS
                Issue tracker at
                .UR https://gitlab.com/islandoftex/albatross/-/issues
                .UE .
                """.trimIndent() + "\n"
            )
        } catch (_: IOException) {
            throw IOException(
                "I could not create the man page for " +
                    "albatross due to an IO error. Please make sure the " +
                    "current directory has the correct permissions " +
                    "and try again. The application will halt now."
            )
        }
    }
}
