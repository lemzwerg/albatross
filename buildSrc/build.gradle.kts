// SPDX-License-Identifier: BSD-3-Clause

repositories {
    mavenCentral()
}

plugins {
    `kotlin-dsl`
}

dependencies {
    "implementation"("org.zeroturnaround:zt-exec:1.11")
}
