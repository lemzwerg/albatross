// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.arguments.transformAll
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.versionOption
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.mordant.rendering.AnsiLevel
import java.time.LocalDate

enum class AlbatrossBorder {
    NONE,
    ASCII,
    ROUNDED,
    BLANK,
    DOUBLE,
    SQUARE,
    HEAVY
}

/**
 * Extends [Regex] to include a check
 * whether the [text] matches the expression.
 * @return a [Boolean] value whether the
 * [text] matches the regular expression.
 */
operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

class Application : CliktCommand(
    name = "albatross",
    printHelpOnEmptyArgs = true,
    help = "A tool for finding fonts that contain a given (Unicode) glyph."
) {
    private val glyphs by argument(name = "glyphs").multiple(required = true).transformAll {
        it.map { input ->
            when (input) {
                in Regex("U\\+[0-9-a-fA-F]+") -> Glyph.fromCodePointOrNull(input.substring(2).uppercase())
                in Regex("0x[0-9-a-fA-F]+") -> Glyph.fromCodePointOrNull(input.substring(2).uppercase())
                else -> Glyph.fromStringOrNull(input)
            } ?: fail(
                "Invalid input, either use\u0085" +
                    "the symbol itself or the corresponding Unicode code point."
            )
        }
    }

    private val showStyles by option("--show-styles", "-s", help = "Show available font styles").flag()
    private val detailed by option("--detailed", "-d", help = "Show a detailed font list").flag()
    private val ansiLevel by option("--ansi-level", "-a", help = "Set the default ANSI level").choice(
        "n" to AnsiLevel.NONE,
        "a16" to AnsiLevel.ANSI16,
        "a256" to AnsiLevel.ANSI256,
        "tc" to AnsiLevel.TRUECOLOR
    ).default(AnsiLevel.NONE)
    private val borderType by option("--border-style", "-b", help = "Set the border style").choice(
        "0" to AlbatrossBorder.NONE,
        "1" to AlbatrossBorder.ASCII,
        "2" to AlbatrossBorder.ROUNDED,
        "3" to AlbatrossBorder.BLANK,
        "4" to AlbatrossBorder.DOUBLE,
        "5" to AlbatrossBorder.SQUARE,
        "6" to AlbatrossBorder.HEAVY
    ).default(AlbatrossBorder.SQUARE)
    private val matchAny by option("--or", "-o", help = "Look for each glyph separately").flag()
    private val includeTeXFonts by option("--include-tex-fonts", "-t", help = "Include fonts from the TeX tree").flag()
    private val clearCache by option("--clear-cache", "-c", help = "Clear the font cache").flag()

    override fun run() {
        val configuration = Configuration(
            showStyles = showStyles,
            detailed = detailed,
            matchAny = matchAny,
            ansiLevel = ansiLevel,
            borderType = borderType,
            includeTeXFonts = includeTeXFonts,
            clearCache = clearCache
        )

        Viewer(configuration, glyphs).view()
    }
}

/**
 * Main application, with [args]
 * as command line arguments.
 */
fun main(args: Array<String>) {
    println(
        """
                __ __           __
        .---.-.|  |  |--.---.-.|  |_.----.-----.-----.-----.
        |  _  ||  |  _  |  _  ||   _|   _|  _  |__ --|__ --|
        |___._||__|_____|___._||____|__| |_____|_____|_____|

        """.trimIndent()
    )

    Application().versionOption(
        Application::class.java.`package`.implementationVersion ?: "DEVELOPMENT BUILD",
        names = setOf("-V", "--version"),
        message = {
            """
            albatross $it
            Copyright (c) ${LocalDate.now().year}, Island of TeX
            All rights reserved.
            """.trimIndent()
        }
    ).main(args)
}
