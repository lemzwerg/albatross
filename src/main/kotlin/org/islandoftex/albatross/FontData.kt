// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.IntArraySerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.absolutePathString

@OptIn(kotlinx.serialization.ExperimentalSerializationApi::class)
class IntRangeArraySerializer : KSerializer<IntRange> {
    private val delegateSerializer = IntArraySerializer()

    override val descriptor: SerialDescriptor
        get() = SerialDescriptor("IntRange", delegateSerializer.descriptor)

    override fun deserialize(decoder: Decoder): IntRange {
        val value = decoder.decodeSerializableValue(delegateSerializer)
        return value[0]..value[1]
    }

    override fun serialize(encoder: Encoder, value: IntRange) {
        encoder.encodeSerializableValue(delegateSerializer, intArrayOf(value.first, value.last))
    }
}

object PathSerializer : KSerializer<Path> {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("Path", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Path {
        val value = decoder.decodeString()
        return Paths.get(value)
    }

    override fun serialize(encoder: Encoder, value: Path) {
        encoder.encodeString(value.absolutePathString())
    }
}

@Serializable
data class FontData(
    @Serializable(with = PathSerializer::class)
    val path: Path,
    val name: String,
    val styles: List<String>,
    val format: String,
    val charset: List<@Serializable(with = IntRangeArraySerializer::class) IntRange>
) {
    /**
     * Provides a string representation.
     * @return the data as a string.
     */
    override fun toString(): String {
        return "($path, $name, $styles, $format)"
    }

    /**
     * Checks whether the [value] is within
     * the charset range.
     * @return whether the [value] is within
     * the charset range.
     */
    fun contains(value: Int) = charset.any { value in it }
}
