// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import java.io.IOException

object CommandLine {

    /**
     * Gets the command output based on the provided [command].
     * @return a list of strings denoting the execution output.
     */
    @Throws(IOException::class)
    fun getCommandOutput(command: List<String>): List<String> {
        return ProcessBuilder(command).start().inputStream
            .bufferedReader().useLines { it.toList() }
    }
}
