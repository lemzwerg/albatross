// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import java.io.IOException
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.absolutePathString
import kotlin.io.path.extension
import kotlin.io.path.walk

/**
 * Extends an [Iterable] by adding a parallel
 * mapping operation based on coroutines.
 * @param [T] the input type.
 * @param [R] the output type.
 * @return a collection of [R] objects.
 */
suspend fun <T, R> Iterable<T>.mapParallel(transform: (T) -> R): List<R> = coroutineScope {
    map { async { transform(it) } }.awaitAll()
}

/**
 * Converts a comma-separated [String] to
 * a proper list of [String].
 * @return a list of [String] split by comma.
 */
fun String.fromCSVtoList() =
    this.takeIf { it.isNotBlank() }?.let { split(",").map { s -> s.trim() } } ?: emptyList()

/**
 * Gets the charset output as [String] and transforms
 * into a list of [IntRange]. If the provided string
 * is blank, a zero-based [IntRange] is returned.
 * @return a list of [IntRange].
 */
fun String.toIntRangeList() = this.takeIf { it.isNotBlank() }?.let { s ->
    s.split(" ").map { a ->
        a.split("-").map {
            it.toInt(Settings.hexRadix)
        }.let {
            it.takeIf { group -> group.size == 1 }?.let { p -> p[0]..p[0] } ?: it[0]..it[1]
        }
    }
} ?: emptyList()

object FontConfig {

    // the window size based on the number
    // of escape chars in the format string
    private val windowSize = Settings.formatFontConfig.count { it == '%' }

    @JvmStatic
    val help: String
        @Throws(IOException::class)
        get() = CommandLine.getCommandOutput(listOf("fc-list", "--help"))[0]

    @JvmStatic
    val isAvailable: Boolean by lazy {
        runCatching { help.isNotBlank() }.getOrElse { false }
    }

    /**
     * Gets a [Map] of font attributes as [String]
     * values based on the provided [charset].
     * @return a [Map] of font attributes.
     * @throws [IOException] in case the
     * command cannot be found.
     */
    @Throws(IOException::class)
    private fun listFonts(charset: String) =
        CommandLine.getCommandOutput(listOf("fc-list", ":charset=$charset", "--format", Settings.formatFontConfig))
            .takeIf { it.size >= windowSize }?.windowed(windowSize, windowSize)?.map {
                it.associate {
                        s ->
                    s.substringBefore("=") to s.substringAfter("=")
                }.toMap()
            } ?: emptyList()

    @OptIn(ExperimentalPathApi::class)
    @Throws(IOException::class)
    private fun queryFonts(path: Path) = runBlocking(Dispatchers.Default) {
        path.walk().filter { it.extension.lowercase() in Settings.fontExtensions }.toList().mapParallel {
            CommandLine.getCommandOutput(
                listOf(
                    "fc-query",
                    it.absolutePathString(),
                    "--format",
                    Settings.formatFontConfig
                )
            ).windowed(windowSize, windowSize).map { w ->
                w.associate { s -> s.substringBefore("=") to s.substringAfter("=") }.toMap()
            }
        }.flatten()
    }

    /**
     * Parses the [output] containing a list of [Map] of
     * font attributes and returns a proper list of
     * [FontData] objects.
     * @return a list of [FontData] objects.
     */
    private fun parse(output: List<Map<String, String>>) =
        output.takeUnless { it.isEmpty() }?.map {
            FontData(
                Paths.get(it["file"]!!),
                it["fullname"]!!.ifBlank { "${it["family"]!!} (family)" },
                it["style"]!!.trim().fromCSVtoList().ifEmpty { listOf("No styles defined") },
                it["fontformat"]!!,
                it["charset"]!!.toIntRangeList()
            )
        } ?: emptyList()

    /**
     * Gets a list of [FontData] objects based
     * on the provided [charset].
     * @return a list of [FontData] objects
     * @throws [IOException] in case the
     * command cannot be found.
     */
    @JvmStatic
    @Throws(IOException::class)
    fun getFonts(charset: String): List<FontData> = parse(listFonts(charset))

    /**
     * Gets a list of [FontData] objects based
     * on the provided [path].
     * @return a list of [FontData] objects
     * @throws [IOException] in case the
     * command cannot be found.
     */
    @JvmStatic
    @Throws(IOException::class)
    fun getFonts(path: Path): List<FontData> = parse(queryFonts(path))

    /**
     * Filters the [list] of [FontData] based on
     * the provided [charset].
     * @return a list of filtered [FontData]
     */
    @JvmStatic
    fun filterFonts(list: List<FontData>, charset: Int) = list.filter { it.contains(charset) }

    /**
     * Gets a list of [FontData] objects based on
     * the provided [path] and the [charset].
     * @return a list of [FontData] objects
     * @throws [IOException] in case the
     * command cannot be found.
     */
    @JvmStatic
    @Throws(IOException::class)
    fun getFonts(path: Path, charset: String): List<FontData> =
        filterFonts(getFonts(path), charset.toInt(Settings.hexRadix))
}
