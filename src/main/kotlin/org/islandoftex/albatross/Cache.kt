// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import kotlinx.serialization.serializer
import net.harawata.appdirs.AppDirsException
import net.harawata.appdirs.AppDirsFactory
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream
import kotlin.io.path.createDirectories
import kotlin.io.path.deleteIfExists
import kotlin.io.path.div
import kotlin.io.path.notExists
import kotlin.io.path.readBytes
import kotlin.io.path.writeBytes

@OptIn(ExperimentalSerializationApi::class)
object Cache {

    private val cacheDirectoryPath: Path by lazy {
        try {
            Paths.get(
                AppDirsFactory.getInstance().getUserCacheDir(
                    "albatross",
                    Application::class.java.`package`.implementationVersion ?: "DEVELOPMENT BUILD",
                    "IoT"
                )
            )
        } catch (_: AppDirsException) {
            Paths.get(".")
        }
    }

    private val cachePath: Path by lazy {
        cacheDirectoryPath / Paths.get(Settings.cacheFileName)
    }

    @JvmStatic
    private val isAvailable: Boolean by lazy { Files.exists(cachePath) }

    /**
     * Builds cache based on both the system
     * and local TeX font directories and
     * gets a list of [FontData] objects.
     * @return [FontData] list.
     */
    private fun buildCacheAndGetFonts() = try {
        if (cacheDirectoryPath.notExists()) { cacheDirectoryPath.createDirectories() }
        val fonts = FontConfig.getFonts(KPSEWhich.getSystemTeXFontsPath()) +
            FontConfig.getFonts(KPSEWhich.getLocalTeXFontsPath())
        val bytes = ProtoBuf.encodeToByteArray(serializer(), fonts)
        cachePath.writeBytes(compress(bytes))
        fonts
    } catch (_: IOException) {
        emptyList()
    }

    /**
     * Reads the cache and returns
     * the corresponding list of
     * [FontData] objects.
     * @return [FontData] list.
     */
    private fun readCache() = try {
        val bytes = decompress(cachePath.readBytes())
        ProtoBuf.decodeFromByteArray<List<FontData>>(bytes)
    } catch (_: IOException) {
        emptyList()
    }

    /**
     * Gets the TeX fonts from the cache.
     * @return [FontData] list from all
     * the fonts found in the system and
     * local directories.
     */
    fun getTeXFonts() = if (isAvailable) readCache() else buildCacheAndGetFonts()

    /**
     * Compresses [bytes] to GZIP format.
     * @return a new [ByteArray].
     */
    private fun compress(bytes: ByteArray): ByteArray =
        ByteArrayOutputStream().use { outputStream ->
            GZIPOutputStream(outputStream).use { gzipStream ->
                gzipStream.write(bytes)
            }
            outputStream.toByteArray()
        }

    /**
     * Decompresses [bytes] to a [ByteArray].
     * @return the original [ByteArray].
     */
    private fun decompress(bytes: ByteArray): ByteArray =
        ByteArrayInputStream(bytes).use { inputStream ->
            GZIPInputStream(inputStream).use { gzipStream ->
                gzipStream.readBytes()
            }
        }

    /**
     * Clear the font cache by removing
     * the file and the version directory.
     */
    fun clearCache() {
        try {
            cachePath.deleteIfExists()
            cacheDirectoryPath.deleteIfExists()
        } catch (_: IOException) {}
    }
}
